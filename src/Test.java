import model.Schedule;
import model.SchedulerBacktracking;
import model.SchedulerIter;

public class Test {
	public static void main(String... args) {
		Schedule schedule = new Schedule();
		int[] lavoro1 = new int[] {0,3};
		int[] lavoro2 = new int[] {1,4};
		int[] lavoro3 = new int[] {2,5};
		int[] lavoro4 = new int[] {3,3};
		int[] lavoro5 = new int[] {4,3};
		
		int[] vincolo1 = new int[] {0,1,1,1,0};
		int[] vincolo2 = new int[] {0,4,1,1,2};
		int[] vincolo3 = new int[] {1,2,0,0,3};//{0,3,1,1,1};
		
		schedule.addLavori(lavoro1, lavoro2, lavoro3, lavoro4, lavoro5);
		schedule.addVincoli(vincolo1, vincolo2, vincolo3);
		schedule.setScheduler(new SchedulerBacktracking());
		schedule.getSchedule();
		
		System.out.println();
		
		Schedule schedule2 = new Schedule();
		schedule2.addLavori(lavoro1, lavoro2, lavoro3, lavoro4, lavoro5);
		schedule2.addVincoli(vincolo1, vincolo2, vincolo3);
		schedule2.setScheduler(new SchedulerIter());
		schedule2.getSchedule();
	
	}
}

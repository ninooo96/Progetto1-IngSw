package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import javafx.scene.control.ScrollBar;
import model.SLObserver;

public class Diagramma extends JPanel implements SLObserver{
//	public JPanel panel ;
	private final int spazio =5;
	
	public Diagramma() {
//		panel= new JPanel();
		setBackground(Color.WHITE);
//		panel.setLayout(new BoxLayout(panel, 1));
//		panel.setPreferredSize( new Dimension(GraphicUI.width-(GraphicUI.width/3), GraphicUI.height));
//		JTextArea jta = new JTextArea();
//		panel.add(new JScrollPane(jta));
//		setSize(new Dimension(GraphicUI.width-(GraphicUI.width/3), GraphicUI.height));
//		Scrollbar scrollHor = new Scrollbar(Scrollbar.HORIZONTAL);
//		Scrollbar scrollVer = new Scrollbar(Scrollbar.VERTICAL);
		
//		setLayout(new BorderLayout());
		
		JLabel tempo = new JLabel("ciao antonio questo è il tempo");
//		add(tempo, BorderLayout.NORTH);
		
//		JPanel diagramma = new JPanel();
//		diagramma.setBackground(Color.red);
		BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
//		GridLayout gl = new GridLayout(2, 1);
		setLayout(bl);
//		add(tempo);
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		add(new JButton("ciaooo"));
//		new JScrollPane(this);
		
	}
	
	public void update(ArrayList<int[]> lavori, HashMap<Integer, Integer> scelte) {
		HashMap<Integer, Integer> scelte1 = scelte;
		HashMap<Integer, Integer> scelte2 = new HashMap<>();
		for( Integer i : scelte1.keySet())
			scelte2.put(i, scelte1.get(i));
		int cont=0;
		for( int[] lavoro : lavori )
			cont+=lavoro[1];
		
		Font font = new Font(Font.MONOSPACED, Font.BOLD, 12);
		Graphics g = getGraphics();
	    FontMetrics metrics = g.getFontMetrics(font);
	    int width = metrics.stringWidth(" ");
	    
//	    Font font = new Font("Lucida Console", Font.PLAIN, 10);
//	    FontMetrics metrics = new FontMetrics(font) {
//	    };
//	    Rectangle2D bounds = metrics.getStringBounds(" ", null);
//	    double width = bounds.getWidth();
	    
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("   0");
		for (int i= 1; i< cont; i++) {
			sBuilder.append(new String(new char[spazio]).replace("\0", " "));
			sBuilder.append(i);
		}

		JLabel primo = new JLabel(sBuilder.toString());
		primo.setFont(font);
		add(primo);
		
		for(Integer id : scelte2.keySet()) {
//			g.fillRect(f*scelte.get(id), 1, f*lavori.get(id)[1], 8);
//			diagramma.add(comp)
			System.out.println(scelte2.get(id)+" scelte");
			
			
		    
			String length = new String(new char[(int) ((width*(spazio))*(scelte2.get(id)+lavori.get(id)[1]))]).replace("\0", " ");
			JLabel label = new JLabel(id+length) {
				
		         public void paintComponent(Graphics g) {
		        	int f = (int) width * (spazio+2);
		            super.paintComponent(g);
//		            setText(new String(new char[] {' '}, 0, f*scelte.get(id)+f*lavori.get(id)[1]));
		            g.setColor(Color.red);
		            
		            g.fillRect((f*(scelte2.get(id))+width*3), 1, (f)*(lavori.get(id)[1]), 30);
//		            double width = this.getSize().getWidth();
//		            double height = this.getSize().getHeight();
		            
//		            for (int i=0; i<width; i+=height) {
//		               g.drawOval(i, 0, (int) height, (int) height);
//		            }
		         }
		      };
//		      label.setBounds(f*scelte.get(id), 1, f*lavori.get(id)[1], 20);
		      
		      label.setFont(font);
		      
		      label.setOpaque(true);
		      
		      add(label);
		      revalidate();
	}
	}

}

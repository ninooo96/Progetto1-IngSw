package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import controller.SLController;

public class GraphicUI extends JFrame{
	private static int widthScreen;
	private static int heightScreen;
	public static int width, height;
	public static Diagramma diagramma;

	public GraphicUI() {
//		JFrame frame = new JFrame();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		widthScreen = (int) screenSize.getWidth();
		heightScreen = (int) screenSize.getHeight();
		setTitle("Schedulazione Lavori");
		setLayout(new GridLayout(1,2));
		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		setSize(new Dimension((int) (widthScreen / 2), (int) (heightScreen / 2)));
		width = getWidth();
		height = getHeight();
		setLocation(new Point((screenSize.width - getSize().width) / 2, 
		(screenSize.height - getSize().height) / 2 ));
		diagramma = new Diagramma();
		JScrollPane jsp = new JScrollPane(diagramma);
//		jsp.setViewportView(diagramma);

		getContentPane().add(jsp);
		
		getContentPane().add(new SLController(diagramma));
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String... args) {
		new GraphicUI();
	}
}

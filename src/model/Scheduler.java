package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import view.Diagramma;

public interface Scheduler { //strategy
//	public List<int[]> lavori;
//	public List<int[]> vincoli = new ArrayList<>();
	public HashMap<Integer, LinkedList<int[]>> vincoliLavoro = new HashMap<>();
	public HashMap<Integer, Integer> scelte = new HashMap<>();
	public List<SLObserver> observers = new ArrayList<>();

	public void schedule(List<int[]> lavori, List<int[]> vincoli );
	
	public void notifyObs(); // dIAGRAMMA  è l'unico observer di Scheduler
	
	default public void addObserver(SLObserver obs) {
		if(!observers.contains(obs))
			observers.add(obs);
	}
	
	default public void removeObserver(SLObserver obs) {
		if(observers.contains(obs))
			observers.remove(obs);
	}
}

package model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SchedulerIterTest {
	private ArrayList<int[]> testLavori, testVincoli;
	private SchedulerIter schedulerIter;

	@Before
	public void setUp() throws Exception {
		testLavori = new ArrayList<>();
		testLavori.add(new int[] {0,3});
		testLavori.add(new int[] {1,4});
		testLavori.add(new int[] {2,5});
		testLavori.add(new int[] {3,3});
		testLavori.add(new int[] {4,3});
		
		testVincoli = new ArrayList<>();
		testVincoli.add(new int[] {0,1,1,1,0});
		testVincoli.add(new int[] {0,4,1,1,2});
		
		schedulerIter = new SchedulerIter();
	}

	@After
	public void tearDown() throws Exception {
		testLavori.clear();
		testVincoli.clear();
	}

	@Test
	public void testSchedule() {
		try {
		schedulerIter.schedule(testLavori, testVincoli);
		assertEquals("Lo scheduler non ha acquisito il corretto numero di lavori", testLavori.size(), schedulerIter.lavori.size());
		assertEquals("Lo scheduler non ha acquisito il corretto numero di vincoli", testVincoli.size(), schedulerIter.vincoli.size());
		schedulerIter.schedule(new ArrayList<>(), new ArrayList<>());
		fail("Lista lavori e lista vincoli vuote");
		}catch(NullPointerException e) {
			e.printStackTrace();
		}
	}

}

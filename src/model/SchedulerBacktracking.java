package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


import backtracking.Problema;
import view.Diagramma;
import view.GraphicUI;

public class SchedulerBacktracking implements Scheduler, Problema <Integer, Integer>{
	public ArrayList<int[]> lavori;
//	private HashMap<Integer, LinkedList<int[]>> vincoliLavoro = new HashMap<>();
//	private HashMap<Integer, Integer> scelte = new HashMap<>(); //lavoro, istante di partenza
	//scelta->l'istante in cui parte il lavoro
	//puntoDiScelta-> il lavoro
	public ArrayList<int[]> vincoli;
	
	//<id-lavoro,  durata>  
	//(  I,  4,  D,  F,  2  )  
	
	
	@Override
	public Integer primoPuntoDiScelta() {
		return lavori.get(0)[0];
	}

	@Override
	public Integer prossimoPuntoDiScelta(Integer ps) {
		return ps+1;
	}

	@Override
	public Integer ultimoPuntoDiScelta() {
		return lavori.get(lavori.size()-1)[0];
	}

	@Override
	public Integer primaScelta(Integer ps) {
		scelte.put(ps, 0);
		return 0;
	}

	@Override
	public Integer prossimaScelta(Integer s) {
		return s+1;
	}

	@Override
	public Integer ultimaScelta(Integer ps) {
		int cont = 0;
		for(int[] lavoro : lavori)
			cont+=lavoro[1];
		return cont-lavori.get(ps)[1]; //Caso peggiore
//		return scelte.get(ps)+lavori.get(ps)[1];
	}

	@Override
	public boolean assegnabile(Integer scelta, Integer puntoDiScelta) {
		java.util.Set<Integer> s = vincoliLavoro.keySet();
//		for(int d : s) {
//			LinkedList<int[]> tmp1 = vincoliLavoro.get(d);
//			System.out.println(d+" ");
//			for( int[] i : tmp1) {
//				System.out.print(i[0]+" "+i[1]+"-");
//			}
//			System.out.println();
//		}
//		System.out.println("iTER");
		boolean flag = true;
		if(!vincoliLavoro.containsKey(puntoDiScelta)) return true;
		LinkedList<int[]> vinc = vincoliLavoro.get(puntoDiScelta);
		
		for(int[] v : vinc) {
			if( (v[1]==puntoDiScelta && v[0]==0)) {//|| (v[4]==puntoDiScelta && v[3]==1)) { //0 significa INIZIO //assumo che i vincoli sono scritti in maniera corretta
//				System.out.println("QUI");
				if(scelta-1 >= (scelte.get(v[4])+lavori.get(v[4])[1])) //istante di partenza maggiore dell'istante finale di idLavoro2(dato dall'istante di inizio + la durata del lavoro)
					flag = true;
				else {
					System.out.println("QUIII");
					return false;
					}
			}
			
			else if( v[4]==puntoDiScelta && v[3]==1) {
//				System.out.println(puntoDiScelta +" "+scelta+" SCELTA "+ (scelte.get(v[1])));
//				System.out.println(scelta+ " SCELTAA");
				if( (scelta==0 || scelte.get(v[1]) == 0) || scelta+1+lavori.get(puntoDiScelta)[1] <= (scelte.get(v[1]))) {
					flag = true;
						
				}
				else return false;
			}
			
			else if( (v[1]==puntoDiScelta && v[0]==1)) {// || (v[4]==puntoDiScelta && v[3]==0)) {//1 significa FINISCE
				System.out.println(puntoDiScelta +" "+v[0]+" "+scelta+" SCELTA "+ (scelte.get(v[4])));

				System.out.println("QUI ENTRA");
				if( ( scelte.get(v[4])==0) || (scelte.get(v[4]) >= scelta-1+lavori.get(v[1])[1] )) flag = true;
				else return false;
			}
			
			else if(v[4]==puntoDiScelta && v[3]==0){
				if( (scelta-1 >= scelte.get(v[1])+lavori.get(v[1])[1])) flag = true;
				else return false;
			}
		}
		return flag;
	}

	@Override
	public void assegna(Integer scelta, Integer puntoDiScelta) {
		
		scelte.put(puntoDiScelta, scelta);
		
	}

	@Override
	public void deassegna(Integer scelta, Integer puntoDiScelta) {
		Integer tmp = scelte.get(puntoDiScelta);
		scelte.put(puntoDiScelta, tmp-scelta);
		
	}

	@Override
	public Integer precedentePuntoDiScelta(Integer puntoDiScelta) {
		return puntoDiScelta-1;
	}

	@Override
	public Integer ultimaSceltaAssegnataA(Integer puntoDiScelta) {
		return scelte.get(puntoDiScelta);
	}

	@Override
	public void scriviSoluzione(int nr_sol) {
		for(int i = 0; i< scelte.size(); i++) {
//			System.out.println(i+"---"+scelte.get(i)+"-"+(scelte.get(i)+lavori.get(i)[1]));
		}
		notifyObs();
		}
	
	public void notifyObs() {
		for(SLObserver obs: observers) {
			obs.update(lavori, scelte);
		}
	}

	@Override
	public void schedule(List<int[]> lavori, List<int[]> vincoli) {
		this.lavori = new ArrayList<>(lavori);
		this.vincoli = new ArrayList<>(vincoli);
		addObserver(GraphicUI.diagramma);
		
		for(int[] lavoro : lavori) {
			scelte.put(lavoro[0], 0);
			for(int[] vincolo : vincoli) {
				if(vincolo[1]==lavoro[0] || vincolo[4] == lavoro[0])
					if(!vincoliLavoro.containsKey(lavoro[0])) {
						LinkedList<int[]> primo = new LinkedList<>();
						primo.addLast(vincolo);
						vincoliLavoro.put(lavoro[0], primo);
					}
					else {
						LinkedList<int[]> tmp = vincoliLavoro.get(lavoro[0]);
						tmp.addLast(vincolo);
						vincoliLavoro.put(lavoro[0], tmp);
					}
			}
			
		}
	Problema.super.risolvi(1);
	}
}
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface SLObserver {
	public void update(ArrayList<int[]> lavori, HashMap<Integer, Integer> scelte);
}

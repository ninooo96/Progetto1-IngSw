package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import view.Diagramma;
import view.GraphicUI;

public class SchedulerIter implements Scheduler {
	public ArrayList<int[]> lavori, vincoli;
//	private HashMap<Integer, LinkedList<int[]>> vincoliLavoro = new HashMap<>();
//	private HashMap<Integer, Integer> scelte = new HashMap<>(); 

	@Override
	public void schedule(List<int[]> lavori, List<int[]> vincoli) {
		initialize(lavori, vincoli);
		for( Integer idL : vincoliLavoro.keySet()) {
			for(int[] v : vincoliLavoro.get(idL)) {
				if( (v[1]==idL && v[0]==0)) {//|| (v[4]==puntoDiScelta && v[3]==1)) { //0 significa INIZIO //assumo che i vincoli sono scritti in maniera corretta
					if(scelte.get(idL)-1 < (scelte.get(v[4])+lavori.get(v[4])[1])) //istante di partenza maggiore dell'istante finale di idLavoro2(dato dall'istante di inizio + la durata del lavoro)
//						continue;
					scelte.put(idL, scelte.get(v[4])+lavori.get(v[4])[1]+1);
				}
				
				if( v[4]==idL && v[3]==1) {
					if(scelte.get(idL)-1 > (scelte.get(v[1]))) {
//							continue;
					scelte.put(v[1], scelte.get(idL)+lavori.get(idL)[1]+1);
					}
					}
				
				if( (v[1]==idL && v[0]==1)) {// || (v[4]==puntoDiScelta && v[3]==0)) {//1 significa FINISCE
					if( scelte.get(v[4]) < scelte.get(idL)-1+lavori.get(v[1])[1] ) //continue;
					scelte.put(v[4], scelte.get(idL)+lavori.get(v[1])[1]+1);
				}
				
				if(v[4]==idL && v[3]==0){
					if( scelte.get(idL)-1 < scelte.get(v[1])+lavori.get(v[1])[1]) //continue;
					scelte.put(v[4], scelte.get(v[1])+lavori.get(v[1])[1]+1);
				}
			}
		}
		
		for(int i = 0; i< scelte.size(); i++) {
			System.out.println(i+"---"+scelte.get(i)+"-"+(scelte.get(i)+lavori.get(i)[1]));
		}
		notifyObs();
		scelte.clear();
		vincoliLavoro.clear();
		
	}
	
	public void notifyObs() {
		for(SLObserver obs: observers) {
			obs.update(lavori, scelte);
		}
	}

	private void initialize(List<int[]> lavori, List<int[]> vincoli) {
		this.lavori = new ArrayList<>(lavori);
		this.vincoli = new ArrayList<>(vincoli);
		addObserver(GraphicUI.diagramma);
		
		for(int[] lavoro : lavori) {
			scelte.put(lavoro[0], 0);
			for(int[] vincolo : vincoli) {
				if(vincolo[1]==lavoro[0] || vincolo[4] == lavoro[0])
					if(!vincoliLavoro.containsKey(lavoro[0])) {
						LinkedList<int[]> primo = new LinkedList<>();
						primo.addLast(vincolo);
						vincoliLavoro.put(lavoro[0], primo);
					}
					else {
						LinkedList<int[]> tmp = vincoliLavoro.get(lavoro[0]);
						tmp.addLast(vincolo);
						vincoliLavoro.put(lavoro[0], tmp);
					}
			}
			
		}
//		
//		for( int[] lavoro : lavori)
//			if(!vincoliLavoro.containsKey(lavoro[0]))
//					scelte.put(lavoro[0], 0);
		
		
		
	}

	
}

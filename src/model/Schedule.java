package model;
import java.util.ArrayList;
import java.util.List;

public class Schedule {
	private List<int[]> lavori, vincoli;
				//vincoli è una lista di array formati da 5 elementi in cui l'INIZIO è pari a 0
				// e la FINE ad 1; il concetto PRIMA è espresso dal valore 0 e DOPO dal valore 1
	private Scheduler scheduler;
	
	public Schedule() {
		lavori = new ArrayList<>();
		vincoli = new ArrayList<>();
		
	}
	
		
	public void addLavori(int[]... lavoro) {//serie di array formati da due elementi
		for(int[] job : lavoro)
			lavori.add(job);
	}
	
	public void addVincoli(int[]... vincolo) {
		for(int[] vinc : vincolo)
			vincoli.add(vinc);
	}
	
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
		
	}
	
	public void getSchedule() {
		scheduler.schedule(lavori, vincoli);
	}
}

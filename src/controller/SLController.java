package controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javafx.scene.text.Text;
import model.Schedule;
import model.Scheduler;
import model.SchedulerBacktracking;
import model.SchedulerIter;
import view.Diagramma;
import view.GraphicUI;


public class SLController extends JPanel{
	public static ArrayList<int[]> lavori = new ArrayList<>();
	public static ArrayList<int[]> vincoli = new ArrayList<>();
//	private final int f = 40;
	private final int spazio =5;
	private Diagramma diagramma;
	
	public SLController(Diagramma diagramma) {
//		JScrollPane jsp = new JScrollPane(diagramma);
		JButton ok1 = new JButton("OK");
		JButton ok2 = new JButton("OK");
		this.diagramma = diagramma;
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(new JLabel("Elenco lavori separati"));
		add(new JLabel("da una virgola"));
		add(new JLabel("Es idL1 durata1, idL2 durata2, etc"));
		JTextField jtf1 = new JTextField();
		add(new JScrollPane(jtf1));
		add(ok1);
		add(new JLabel("-----------------"));
		add(new JLabel("Inserisci i vincoli separati"));
		add(new JLabel("da una virgola"));
		add(new JLabel("Es I/F idL1 P/D I/F idL2"));
		JTextField jtf2 = new JTextField();
		jtf2.setAutoscrolls(true);
		add(new JScrollPane(jtf2));
		add(ok2);
		
		JButton avviaIter = new JButton("Avvia Approccio Iterativo");
		add(avviaIter);
		
		JButton avviaBacktr = new JButton("Avvia approccio Backtracking");
		add(avviaBacktr);
		
		Schedule schedule = new Schedule();
		
		ok1.addActionListener((ActionEvent e)->{
			try {
				String richiesta = jtf1.getText();
				int ind=0;
				ArrayList<String> lavoriTmp = new ArrayList<>();
				StringTokenizer st = new StringTokenizer(richiesta,",");
				while(st.hasMoreTokens()) {
					lavoriTmp.add(ind, st.nextToken());
					ind++;
				}
				lavori.clear();
				for(int i = 0;i < lavoriTmp.size(); i++) {
					StringTokenizer st2 = new StringTokenizer(lavoriTmp.get(i), " ");
					if(!(st2.countTokens()==2)) throw new IllegalStateException();
					int[] tmp = new int[2];
					tmp[0] = Integer.parseInt(st2.nextToken());
					tmp[1] = Integer.parseInt(st2.nextToken());
					lavori.add(i, tmp);
				}
				
				for(int[] lavoro: lavori) {
					schedule.addLavori(lavoro);
					System.out.println(lavoro[0]+"-"+lavoro[1]+" LAVORI");
				}
				
				for(int[] lavoro: lavori) {
					System.out.println(lavoro[0]+"-uffa-"+lavoro[1]);
				}
			}catch(IllegalStateException exc) {
				JOptionPane.showMessageDialog(null, "Controlla il formato");
				}
		});
		
		ok2.addActionListener((ActionEvent e)->{
			try {
				String richiesta = jtf2.getText();
				int ind=0;
				ArrayList<String> vincoliTmp = new ArrayList<>();
				StringTokenizer st = new StringTokenizer(richiesta,",");
				while(st.hasMoreTokens()) {
					vincoliTmp.add(ind, st.nextToken());
					ind++;
				}
				vincoli.clear();
				for(int i = 0;i < vincoliTmp.size(); i++) {
					StringTokenizer st2 = new StringTokenizer(vincoliTmp.get(i), " ");
					int[] tmp2 = new int[5];
					if(!(st2.countTokens()==5)) throw new IllegalStateException();
					
					String temp0 = st2.nextToken();
					if(temp0.equalsIgnoreCase("I")) tmp2[0]=0;
					else if(temp0.equalsIgnoreCase("F")) tmp2[0] = 1;
					else throw new IllegalStateException();
					
					tmp2[1] = Integer.parseInt(st2.nextToken());
					
					String temp1 = st2.nextToken();
					if(temp1.equalsIgnoreCase("P")) tmp2[2]=0;
					else if(temp1.equalsIgnoreCase("D")) tmp2[2]=1;
					else throw new IllegalStateException();
					
					String temp2 = st2.nextToken();
					if(temp2.equalsIgnoreCase("I")) tmp2[3]=0;
					else if(temp2.equalsIgnoreCase("F")) tmp2[3]=1;
					else throw new IllegalStateException();
					
					tmp2[4] = Integer.parseInt(st2.nextToken());
					vincoli.add(i, tmp2);
					
					
				}
				for(int[] vincolo : vincoli) {
					schedule.addVincoli(vincolo);
					System.out.println(vincolo[0]+"-"+vincolo[1]+"-"+vincolo[2]+"-"+vincolo[3]+"-"+ vincolo[4]+" VINCOLI");
				}
			}catch(IllegalStateException exc) {
				JOptionPane.showMessageDialog(null, "Controlla il formato");
				}
		});
		
		avviaIter.addActionListener((ActionEvent e)->{
//			Schedule schedule = new Schedule();
		
//			for(int[] lavoro: lavori) {
//				schedule.addLavori(lavoro);
//				System.out.println(lavoro[0]+"-"+lavoro[1]+" LAVORI");
//			}
//			for(int[] vincolo : vincoli) {
//				schedule.addVincoli(vincolo);
//				System.out.println(vincolo[0]+"-"+vincolo[1]+"-"+vincolo[2]+"-"+vincolo[3]+"-"+ vincolo[4]+" VINCOLI");
//			}
			SchedulerIter schedulerIter = new SchedulerIter();
			schedule.setScheduler(schedulerIter);	
			schedule.getSchedule();
//			drawDiagram(schedulerIter);
			
		});
		
		avviaBacktr.addActionListener((ActionEvent e)->{
//			Schedule schedule = new Schedule();
//			
//			for(int[] lavoro: lavori) {
//				schedule.addLavori(lavoro);
//				System.out.println(lavoro[0]+"-"+lavoro[1]+" LAVORI");
//			}
//			for(int[] vincolo : vincoli) {
//				schedule.addVincoli(vincolo);
//				System.out.println(vincolo[0]+"-"+vincolo[1]+"-"+vincolo[2]+"-"+vincolo[3]+"-"+ vincolo[4]+" VINCOLI");
//			}
			SchedulerBacktracking schedulerBacktr = new SchedulerBacktracking();
			schedule.setScheduler(schedulerBacktr);	
			schedule.getSchedule();
		});
		
	}
	
	private void drawDiagram(Scheduler scheduler) {
//		HashMap<Integer, Integer> scelte1 = scheduler.scelte;
//		HashMap<Integer, Integer> scelte = new HashMap<>();
//		for( Integer i : scelte1.keySet())
//			scelte.put(i, scelte1.get(i));
//		int cont=0;
//		for( int[] lavoro : lavori )
//			cont+=lavoro[1];
//		
//		Font font = new Font(Font.MONOSPACED, Font.BOLD, 12);
//		Graphics g = this.getGraphics();
//	    FontMetrics metrics = g.getFontMetrics(font);
//	    int width = metrics.stringWidth(" ");
//	    
////	    Font font = new Font("Lucida Console", Font.PLAIN, 10);
////	    FontMetrics metrics = new FontMetrics(font) {
////	    };
////	    Rectangle2D bounds = metrics.getStringBounds(" ", null);
////	    double width = bounds.getWidth();
//	    
//		StringBuilder sBuilder = new StringBuilder();
//		sBuilder.append('0');
//		for (int i= 1; i< cont; i++) {
//			sBuilder.append(new String(new char[spazio]).replace("\0", " "));
//			sBuilder.append(i);
//		}
//
//		JLabel primo = new JLabel(sBuilder.toString());
//		primo.setFont(font);
//		diagramma.panel.add(primo);
//		
//		for(Integer id : scelte.keySet()) {
////			g.fillRect(f*scelte.get(id), 1, f*lavori.get(id)[1], 8);
////			diagramma.add(comp)
//			System.out.println(scelte.get(id)+" scelte");
//			
//			
//		    
//			String length = new String(new char[(int) ((width*(spazio+1))*(scelte.get(id)+lavori.get(id)[1]))]).replace("\0", " ");
//			JLabel label = new JLabel(length) {
//				
//		         public void paintComponent(Graphics g) {
//		        	int f = (int) width * (spazio+2);
//		            super.paintComponent(g);
////		            setText(new String(new char[] {' '}, 0, f*scelte.get(id)+f*lavori.get(id)[1]));
//		            g.setColor(Color.red);
//		            g.fillRect((f)*(scelte.get(id)), 1, (f)*(lavori.get(id)[1]), 30);
////		            double width = this.getSize().getWidth();
////		            double height = this.getSize().getHeight();
//		            
////		            for (int i=0; i<width; i+=height) {
////		               g.drawOval(i, 0, (int) height, (int) height);
////		            }
//		         }
//		      };
////		      label.setBounds(f*scelte.get(id), 1, f*lavori.get(id)[1], 20);
//		      
//		      label.setFont(font);
//		      
//		      label.setOpaque(true);
//		      
//		      diagramma.panel.add(label);
//		      diagramma.panel.revalidate();
		}
		
//		diagramma.panel.add(new Label());
//		diagramma.panel.revalidate();
	
}
